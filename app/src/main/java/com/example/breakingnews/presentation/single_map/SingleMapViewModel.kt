package com.example.breakingnews.presentation.single_map

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.repository.PostRepository
import com.example.breakingnews.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SingleMapViewModel @Inject constructor(
    private val repository: PostRepository
): ViewModel() {

    var state by mutableStateOf(SingleMapState())

    fun onEvent(event: SingleMapEvent) {
        when (event) {
            is SingleMapEvent.Refresh -> {
                    getPosts(userID = state.userId, location = state.location)
                }
            is SingleMapEvent.OnLocationChange -> {
                state = state.copy(
                    location = event.location
                )
            }
        }
    }

    fun getPosts(
        userID: String = state.userId,
        location: Location = state.location
    ) {
        viewModelScope.launch {
            repository
                .getPosts(userID, location)
                .collect {result ->
                    when(result) {
                        is Resource.Success -> {
                            result.data?.let {posts ->
                                state = state.copy(
                                    posts = posts
                                )
                            }
                        }
                        is Resource.Error -> Unit
                        is Resource.Loading -> {

                        }
                    }
                }
        }
    }
}

