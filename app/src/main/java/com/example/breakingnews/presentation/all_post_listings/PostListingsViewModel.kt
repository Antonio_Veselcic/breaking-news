package com.example.breakingnews.presentation.all_post_listings

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.model.Post
import com.example.breakingnews.domain.repository.PostRepository
import com.example.breakingnews.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostListingsViewModel @Inject constructor(
    private val repository: PostRepository
): ViewModel() {

    var state by mutableStateOf(PostListingsState())

    val visiblePermissionDialogQueue = mutableStateListOf<String>()

    fun onEvent(event: PostListingsEvent) {
        when (event) {
            is PostListingsEvent.Refresh -> {
                getPosts(userID = state.userId, location = state.location)
            }
            is PostListingsEvent.OnLocationChange -> {
                state = state.copy(
                    location = event.location
                )
            }
            is PostListingsEvent.LocationToggle -> {
                state = state.copy(
                    isLocationOn = !state.isLocationOn
                )
                this.onEvent(PostListingsEvent.Refresh(event.context))
            }
            is PostListingsEvent.DeletePost -> {
                deletePost(Location(event.lat, event.lon))
            }
        }
    }

    fun getPosts(
        userID: String = state.userId,
        location: Location = state.location
    ) {
        viewModelScope.launch {
            repository
                .getPosts(userID, location)
                .collect {result ->
                    when(result) {
                        is Resource.Success -> {
                            result.data?.let {posts ->
                                state = state.copy(
                                    posts = posts
                                )
                            }
                        }
                        is Resource.Error -> Unit
                        is Resource.Loading -> {
                            state = state.copy(isLoading = result.isLoading)
                        }
                    }
                }
        }
    }

    private fun deletePost(
        location: Location
    ) {
        var posts = state.posts

        var newPosts = mutableListOf<Post>()

        posts.forEach { post -> {
            if (post.location.latitude != location.latitude && post.location.longitude != location.longitude) {
                newPosts.add(post)
            }
        }}

        state = state.copy(
            posts = newPosts
        )

        repository.deletePost(location)
    }

    fun dismissDialog() {
        visiblePermissionDialogQueue.removeLast()
    }

    fun onPermissionResult(
        permission: String,
        isGranted: Boolean
    ) {
        if(!isGranted) {
            visiblePermissionDialogQueue.add(0, permission)
        }
    }
}

