package com.example.breakingnews.presentation.all_post_listings

import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.model.Post

data class PostListingsState(
    val posts: List<Post> = emptyList(),
    val isLoading: Boolean = false,
    val isRefreshing: Boolean = false,
    val isLocationOn: Boolean = true,
    val userId: String = "",
    val location: Location = Location(0.0, 0.0)
)
