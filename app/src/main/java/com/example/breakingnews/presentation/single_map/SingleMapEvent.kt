package com.example.breakingnews.presentation.single_map

import android.content.Context
import com.example.breakingnews.data.remote.location.Location

sealed class SingleMapEvent {

    data class Refresh(val context: Context): SingleMapEvent()

    data class OnLocationChange(val location: Location): SingleMapEvent()
}
