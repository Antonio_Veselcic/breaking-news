package com.example.breakingnews.presentation.add_post

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.breakingnews.data.data_types.PostEntity
import com.example.breakingnews.domain.repository.PostRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddPostViewModel @Inject constructor(
    private val repository: PostRepository
): ViewModel() {

    var state by mutableStateOf(AddPostState())

    fun onEvent(event: AddPostEvent) {
        when(event) {
            is AddPostEvent.AddPost -> {
                Log.d("", "" + state.location.latitude + state.location.longitude)
                val postEntity = PostEntity(
                    userID = state.userID,
                    imageUri = state.imageUri,
                    title = state.title,
                    description = state.description,
                    latitude = state.location.latitude,
                    longitude = state.location.longitude,
                    showLocation = state.showLocation,
                    date = state.date
                )
                uploadPost(postEntity)
            }
            is AddPostEvent.OnTitleChange -> {
                state = state.copy(title = event.title)
            }
            is AddPostEvent.OnDescriptionChange -> {
                state = state.copy(description = event.description)
            }
            is AddPostEvent.OnImageChange -> {
                state = state.copy(imageUri = event.imageUri)
            }
            is AddPostEvent.OnStart -> {
                state = state.copy(
                    userID = event.userID,
                )
            }
            is AddPostEvent.OnLocationChange -> {
                state = state.copy(
                    location = event.location
                )
            }
            is AddPostEvent.OnShowLocationChange -> {
                state = state.copy(showLocation = event.showLocation)
            }
        }
    }

    private fun uploadPost(postEntity: PostEntity) {
        GlobalScope.launch{
            repository.uploadPost(postEntity);
        }
    }
}