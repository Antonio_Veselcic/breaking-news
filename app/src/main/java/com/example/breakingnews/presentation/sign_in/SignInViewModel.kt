package com.example.breakingnews.presentation.sign_in

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import com.example.breakingnews.data.remote.authentication.SignInResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

// ubrizgavanje bilo cega u ovaj konstruktor dagger hiltom crasha cijelu aplikaciju
@HiltViewModel
class SignInViewModel @Inject constructor(
    //postRepositoryImplementation: PostRepositoryImplementation
): ViewModel() {

    private val _state = MutableStateFlow(SignInState())
    val state = _state.asStateFlow()
    val visiblePermissionDialogQueue = mutableStateListOf<String>()

    fun onSignInResult(result: SignInResult) {
        _state.update {it.copy(
            isSignInSuccessful = result.data != null,
            signInError = result.errorMessage
        ) }
    }

    fun resetState() {
        _state.update {
            SignInState()
        }
    }

    fun dismissDialog() {
        visiblePermissionDialogQueue.removeFirst()
    }

    fun onPermissionResult(
        permission: String,
        isGranted: Boolean
    ) {
        if(!isGranted && !visiblePermissionDialogQueue.contains(permission)) {
            visiblePermissionDialogQueue.add(permission)
        }
    }
}
