package com.example.breakingnews.presentation.single_map

import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.model.Post

data class SingleMapState(
    val posts: List<Post> = emptyList(),
    val userId: String = "",
    val location: Location = Location(0.0, 0.0)
)
