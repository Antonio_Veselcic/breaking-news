package com.example.breakingnews.presentation.add_post

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Looper
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.breakingnews.data.remote.location.Location
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddPost(
    navController: NavController,
    userId: String,
    viewModel: AddPostViewModel = hiltViewModel(),
) {
    var context = LocalContext.current

    var state = viewModel.state

    var selectedImageUri by remember {
        mutableStateOf<Uri?>(null)
    }
    val singlePhotoPickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.PickVisualMedia(),
        onResult = { uri ->
            selectedImageUri = uri
            state = state.copy (
                imageUri = uri.toString()
            )
        }
    )

    var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    var locationCallback = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult) {
            for (lo in p0.locations) {
                viewModel.onEvent(AddPostEvent.OnLocationChange(Location(lo.latitude, lo.longitude)))
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        locationCallback?.let {
            val locationRequest = LocationRequest.create().apply {
                interval = 10000
                fastestInterval = 5000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            fusedLocationClient?.requestLocationUpdates(
                locationRequest,
                it,
                Looper.getMainLooper()
            )
        }
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        viewModel.onEvent(AddPostEvent.OnStart(userId))
        startLocationUpdates()
        if(selectedImageUri != null) {
            Text(
                text = "Selected image: ",
                modifier = Modifier
                    .padding(8.dp)
            )
            AsyncImage(
                model = selectedImageUri,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(300.dp)
                    .border(1.dp, MaterialTheme.colorScheme.onPrimary),
            )
        }
        Button(
            modifier = Modifier
                .padding(8.dp),
            onClick = {
            singlePhotoPickerLauncher.launch(
                PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly)
            )
        }) {
            if (selectedImageUri == null) {
                Text(text = "Pick a photo")
            } else {
                Text(text = "Pick another photo")
                viewModel.onEvent(AddPostEvent.OnImageChange(selectedImageUri.toString()))
            }
        }

        OutlinedTextField(
            value = "" + state.title,
            onValueChange = {
                viewModel.onEvent(AddPostEvent.OnTitleChange(it))
            },
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            placeholder = {
                Text(text = "Title...")
            },
            maxLines = 2,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = MaterialTheme.colorScheme.primary,
                unfocusedBorderColor = MaterialTheme.colorScheme.primary,
                textColor = MaterialTheme.colorScheme.onBackground)
        )

        OutlinedTextField(
            value = "" + state.description,
            onValueChange = {
                viewModel.onEvent(AddPostEvent.OnDescriptionChange(it))
            },
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            placeholder = {
                Text(text = "Description...")
            },
            maxLines = 10,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = MaterialTheme.colorScheme.primary,
                unfocusedBorderColor = MaterialTheme.colorScheme.primary,
                textColor = MaterialTheme.colorScheme.onBackground)
        )

        Row (
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = "Add location tag?")
            Checkbox(
                checked = state.showLocation,
                onCheckedChange = { viewModel.onEvent(AddPostEvent.OnShowLocationChange(it)) }
            )

            Button(
                modifier = Modifier
                    .padding(8.dp),
                onClick = {
                    if (ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        viewModel.onEvent(AddPostEvent.AddPost)
                        navController.popBackStack()
                    } else {
                        Toast.makeText(context, "Can't post with your location disabled.", Toast.LENGTH_LONG).show()
                    }
                }
            ) {
                Text(text = "Post")
            }
        }
    }
}