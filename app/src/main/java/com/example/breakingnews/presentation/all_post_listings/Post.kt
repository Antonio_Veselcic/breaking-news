package com.example.breakingnews.presentation.all_post_listings

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Map
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.breakingnews.domain.model.Post
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import java.text.SimpleDateFormat

@Composable
fun Post(
    post: Post,
    modifier: Modifier = Modifier,
    screenType: ScreenType,
    onButtonClick: (Double, Double) -> Unit,
    onDeleteButton: (Double, Double) -> Unit,
    context: Context,
    userId: String
) {
    var isMapOpened = remember { mutableStateOf(false)}

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Max)
            .background(MaterialTheme.colorScheme.background)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = post.title,
                fontSize = 24.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier
                    .padding(8.dp)
                    .weight(0.8f),
                color = MaterialTheme.colorScheme.onBackground
            )
            if (post.showLocation) {
                Button(
                    onClick = {
                        onButtonClick(post.location.latitude, post.location.longitude)
                        if (screenType == ScreenType.ALL_POSTS) {
                            isMapOpened.value = !isMapOpened.value
                        }
                    },
                    modifier = Modifier
                        .padding(8.dp)
                        .weight(0.2f)
                ) {
                    Icon(
                        Icons.Filled.Map,
                        contentDescription = "Open map",
                        tint = MaterialTheme.colorScheme.onTertiary
                    )
                }
            }
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = SimpleDateFormat("dd/MM/yyyy").format(post.date),
                fontSize = 16.sp,
                modifier = Modifier
                    .padding(8.dp)
                    .weight(0.8f),
                color = MaterialTheme.colorScheme.onBackground,
            )
            if (post.userID == post.userID) {
                Button(
                    onClick = {
                        onDeleteButton(post.location.latitude, post.location.longitude)
                    },
                    modifier = Modifier
                        .padding(8.dp)
                        .weight(0.2f)
                ) {
                    Icon(
                        Icons.Filled.Delete,
                        contentDescription = "Delete",
                        tint = MaterialTheme.colorScheme.onTertiary
                    )
                }
            }
        }
        if (post.description != null) {
            Text(
                text = post.description,
                //maxLines = 5,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.padding(8.dp),
                color = MaterialTheme.colorScheme.onBackground
            )
        }
        if (isMapOpened.value) {
            val location = LatLng(
                post.location.latitude,
                post.location.longitude
            )
            val cameraPositionState = rememberCameraPositionState {
                position = CameraPosition.fromLatLngZoom(location, 16f)
            }
            GoogleMap(
                modifier = Modifier.height(200.dp),
                cameraPositionState = cameraPositionState,
            ) {
                Marker(
                    state = rememberMarkerState(position = location),
                    icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)
                )
            }
        }
        if (!post.imageUri.isNullOrEmpty()) {
            AsyncImage(
                model = post.imageUri,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .requiredHeight(150.dp)
                    .fillMaxWidth(),
                )
        }
    }
}

enum class ScreenType {
    ALL_POSTS, PROFILE;
}
