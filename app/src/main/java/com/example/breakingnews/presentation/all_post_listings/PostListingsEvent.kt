package com.example.breakingnews.presentation.all_post_listings

import android.content.Context
import com.example.breakingnews.data.remote.location.Location

sealed class PostListingsEvent {

    data class LocationToggle(val context: Context): PostListingsEvent()
    data class Refresh(val context: Context): PostListingsEvent()

    data class OnLocationChange(val location: Location): PostListingsEvent()
    data class DeletePost(val lat: Double, val lon: Double): PostListingsEvent()
}
