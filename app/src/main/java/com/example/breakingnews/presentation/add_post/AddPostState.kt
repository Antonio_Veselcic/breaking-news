package com.example.breakingnews.presentation.add_post

import com.example.breakingnews.data.remote.location.Location
import java.util.Calendar
import java.util.Date

data class AddPostState(
    val userID: String = "",
    val imageUri: String = "",
    val title: String = "",
    val description: String? = "",
    val location: Location = Location(0.0,0.0),
    val showLocation: Boolean = false,
    val date: Date = Calendar.getInstance().time
)
