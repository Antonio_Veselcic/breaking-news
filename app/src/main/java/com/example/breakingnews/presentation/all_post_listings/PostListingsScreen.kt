package com.example.breakingnews.presentation.all_post_listings

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Looper
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.CalendarMonth
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.presentation.all_post_listings.location_tracker.LocationService
import com.example.breakingnews.util.navigation.Screens
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.Date
import kotlin.math.sqrt

@RequiresApi(Build.VERSION_CODES.O)
@SuppressLint("Range")
@Composable
fun PostListingsScreen(
    userId: String,
    navController: NavController,
    viewModel: PostListingsViewModel = hiltViewModel()
) {
    val swipeRefreshState = rememberSwipeRefreshState(
        isRefreshing = viewModel.state.isRefreshing
    )
    val isLocationOnState = rememberSwipeRefreshState(
        isRefreshing = viewModel.state.isLocationOn
    )
    val state = viewModel.state

    val context = LocalContext.current
    val dialogQueue = viewModel.visiblePermissionDialogQueue

    val launcher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            Log.d("PostListingsScreen","PERMISSION GRANTED")
        } else {
            Log.d("PostListingsScreen","PERMISSION DENIED")
        }
    }

    var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    var locationCallback = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult) {
            for (lo in p0.locations) {
                viewModel.onEvent(PostListingsEvent.OnLocationChange(Location(lo.latitude, lo.longitude)))
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        locationCallback?.let {
            val locationRequest = LocationRequest.create().apply {
                interval = 10000
                fastestInterval = 5000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            fusedLocationClient?.requestLocationUpdates(
                locationRequest,
                it,
                Looper.getMainLooper()
            )
        }
    }

    val locationPermissionResultLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestMultiplePermissions(),
        onResult = { permissions ->
            permissions.keys.forEach { permission ->
                viewModel.onPermissionResult(
                    permission = permission,
                    isGranted = permissions[permission] == true
                )
            }
        }
    )

    LaunchedEffect(Unit) {
        while (state.posts.isEmpty()) {
            locationPermissionResultLauncher.launch(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
            startLocationUpdates()
            delay(1000)
            viewModel.onEvent(PostListingsEvent.Refresh(context))
        }
    }

    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            SwipeRefresh(
                state = swipeRefreshState,
                onRefresh = {
                    viewModel.onEvent(
                        PostListingsEvent.Refresh(context)
                    )
                }
            ) {
                LazyColumn(
                    modifier = Modifier.fillMaxSize()
                ) {
                    var sortedPosts = state.posts.sortedByDescending { it.date }
                    if (!state.isLocationOn) {
                        sortedPosts = state.posts.sortedBy {
                            calculateDistance(it.location.latitude, it.location.longitude, state.location.latitude, state.location.longitude)
                        }
                    }

                    items(sortedPosts.size) { i ->
                        val post = sortedPosts[i]
                        var x = post.location.latitude
                        var y = post.location.longitude
                        if ((post.location.latitude >= state.location.latitude - 10.0 && post.location.latitude <= state.location.latitude + 10.0) &&
                            (post.location.longitude >= state.location.longitude - 10.0 && post.location.longitude <= state.location.longitude + 10.0)
                            && !isOlderThanAWeek(post.date)
                        ) {
                            Post(
                                post = post,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(16.dp),
                                ScreenType.ALL_POSTS,
                                onButtonClick = { x, y ->  println("Lambda poziv iz PostListingsScreen: " + x + ", " + y) },
                                onDeleteButton = { x, y ->
                                    viewModel.onEvent(PostListingsEvent.DeletePost(x, y))
                                    CoroutineScope(Dispatchers.Main).launch {
                                        delay(500) // 1 second delay
                                        viewModel.onEvent(PostListingsEvent.Refresh(context))
                                    }
                                  },
                                context = context,
                                userId = userId
                            )
                            Divider(
                                thickness = 1.dp,
                                color = MaterialTheme.colorScheme.primary
                            )
                        }
                    }
                }
            }
        }

        // gumb i ikona su odvojene composable komponente
        // ovo je jako glupo i lose, ali previse vremena sam izgubio pokusavajuci narediti ikonu dobre velicine u gumb.
        // ako je ikona unutar buttona on je ogranici, bude sitna i izgleda smijesno, ne vidi se sto je na ikoni
        Button(
            onClick = {
                locationPermissionResultLauncher.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                )

                if (ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    viewModel.onEvent(PostListingsEvent.LocationToggle(context))
                    viewModel.onEvent(PostListingsEvent.Refresh(context))
                    isLocationOnState.isRefreshing = !isLocationOnState.isRefreshing

                    Intent(context, LocationService::class.java).apply {
                        this.putExtra("userID", userId)
                        action = LocationService.ACTION_START
                        context.startService(this)
                    }
                }
            },
            modifier = Modifier
                .size(90.dp)
                .padding(16.dp)
                .align(Alignment.BottomStart),
            shape = CircleShape
            ) {

        }

        Icon(
            if (state.isLocationOn) {
                Icons.Filled.CalendarMonth
            } else {
                Icons.Filled.LocationOn
            },
            contentDescription = "Request location permission",
            modifier = Modifier
                .size(88.dp)
                .padding(29.dp)
                .align(Alignment.BottomStart),
            tint = MaterialTheme.colorScheme.onTertiary
        )

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            Button(
                onClick = {
                    navController.navigate(Screens.SingleMap.route)
                },
                modifier = Modifier
                    .height(90.dp)
                    .padding(16.dp)
                    .align(Alignment.BottomCenter),
                shape = RoundedCornerShape(50)
            ) {
                Text("Open map of the area")
            }
        }

        Button(
            onClick = {
                navController.navigate(Screens.AddPost.route)
            },
            modifier = Modifier
                .size(90.dp)
                .padding(16.dp)
                .align(Alignment.BottomEnd),
            shape = CircleShape
        ) {

        }

        Icon(
            Icons.Filled.Add,
            contentDescription = "Request location permission",
            modifier = Modifier
                .size(88.dp)
                .padding(29.dp)
                .align(Alignment.BottomEnd),
            tint = MaterialTheme.colorScheme.onTertiary
        )
    }

    dialogQueue
        .reversed()
        .forEach { permission ->
            PermissionDialog(
                permissionTextProvider = when (permission) {
                    Manifest.permission.CAMERA -> {
                        CameraPermissionTextProvider()
                    }
                    Manifest.permission.RECORD_AUDIO -> {
                        RecordAudioPermissionTextProvider()
                    }
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION -> {
                        LocationPermissionTextProvider()
                    }
                    Manifest.permission.READ_CONTACTS -> {
                        ContactPermissionTextProvider()
                    }
                    else -> return@forEach
                },
                isPermanentlyDeclined = !ActivityCompat.shouldShowRequestPermissionRationale(
                    context as Activity,
                    permission,
                ),
                onDismiss = viewModel::dismissDialog,
                onOkClick = {
                    viewModel.dismissDialog()
                },
            )
        }

}

fun calculateDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
    val dLat = lat2 - lat1
    val dLon = lon2 - lon1
    return sqrt(dLat * dLat + dLon * dLon)
}

@RequiresApi(Build.VERSION_CODES.O)
fun isOlderThanAWeek(postDate: Date): Boolean {
    val postDateTime = postDate.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()
    val oneWeekAgo = LocalDateTime.now().minus(1, ChronoUnit.WEEKS)
    return postDateTime.isBefore(oneWeekAgo)
}