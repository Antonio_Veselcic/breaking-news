package com.example.breakingnews.presentation.add_post

import com.example.breakingnews.data.remote.location.Location

sealed class AddPostEvent {
    object AddPost: AddPostEvent()
    data class OnTitleChange(val title: String): AddPostEvent()
    data class OnDescriptionChange(val description: String): AddPostEvent()
    data class OnImageChange(val imageUri: String): AddPostEvent()
    data class OnStart(val userID: String): AddPostEvent()
    data class OnLocationChange(val location: Location): AddPostEvent()
    data class OnShowLocationChange(val showLocation: Boolean): AddPostEvent()
}
