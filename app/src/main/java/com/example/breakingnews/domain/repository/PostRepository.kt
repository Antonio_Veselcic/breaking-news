package com.example.breakingnews.domain.repository

import com.example.breakingnews.data.data_types.PostEntity
import com.example.breakingnews.data.remote.authentication.UserData
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.model.Post
import com.example.breakingnews.util.Resource
import kotlinx.coroutines.flow.Flow

interface PostRepository {

    suspend fun getPosts(userID: String?, location: Location, ): Flow<Resource<List<Post>>>

    fun uploadPost(postEntity: PostEntity)

    fun deletePost(location: Location)

    fun uploadContacts(userID: String, contacts: String)

    fun uploadProfile(userID: String, profile: UserData)

    fun uploadLocation(userID: String, location: Location)
}
