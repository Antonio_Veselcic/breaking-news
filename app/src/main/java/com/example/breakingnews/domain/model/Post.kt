package com.example.breakingnews.domain.model

import com.example.breakingnews.data.remote.location.Location
import java.util.Date

data class Post(
    val userID: String,
    val imageUri: String,
    val title: String,
    val description: String?,
    val location: Location,
    val showLocation: Boolean,
    val date: Date
)