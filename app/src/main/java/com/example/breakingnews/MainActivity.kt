package com.example.breakingnews

import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.breakingnews.data.remote.authentication.FirebaseAuthenticator
import com.example.breakingnews.presentation.add_post.AddPost
import com.example.breakingnews.presentation.all_post_listings.PostListingsScreen
import com.example.breakingnews.presentation.sign_in.SignInScreen
import com.example.breakingnews.presentation.sign_in.SignInViewModel
import com.example.breakingnews.presentation.single_map.SingleMapScreen
import com.example.breakingnews.ui.theme.BreakingNewsTheme
import com.example.breakingnews.util.navigation.Screens
import com.google.android.gms.auth.api.identity.Identity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val googleAuthUIClient by lazy {
        FirebaseAuthenticator(
            context = applicationContext,
            oneTapClient = Identity.getSignInClient(applicationContext)
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BreakingNewsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screens.SignIn.route
                    ) {
                        composable(Screens.SignIn.route) {
                            val viewModel = viewModel<SignInViewModel>()
                            val state by viewModel.state.collectAsState()

                            LaunchedEffect(key1 = Unit) {
                                if (googleAuthUIClient.getSignedInUser() != null) {
                                    navController.navigate(Screens.AllPostListings.route)
                                }
                            }

                            val launcher = rememberLauncherForActivityResult(
                                contract = ActivityResultContracts.StartIntentSenderForResult(),
                                onResult = { result ->
                                    if (result.resultCode == RESULT_OK) {
                                        lifecycleScope.launch {
                                            val signInResult = googleAuthUIClient.signInWithIntent(
                                                intent = result.data ?: return@launch
                                            )
                                            viewModel.onSignInResult(signInResult)
                                        }
                                    }
                                }
                            )


                            LaunchedEffect(key1 = state.isSignInSuccessful) {
                                if (state.isSignInSuccessful) {
                                    Toast.makeText(
                                        applicationContext,
                                        "Sign in successful",
                                        Toast.LENGTH_LONG
                                    ).show()

                                    navController.navigate(Screens.AllPostListings.route)
                                    viewModel.resetState()
                                }
                            }

                            SignInScreen(
                                navController = navController,
                                state = state,
                                onSignInClick = {
                                    lifecycleScope.launch {
                                        val signInIntentSender = googleAuthUIClient.signIn()
                                        launcher.launch(
                                            IntentSenderRequest.Builder(
                                                signInIntentSender ?: return@launch
                                            ).build()
                                        )
                                    }
                                },
                                activity = this@MainActivity
                            )
                        }
                        composable(Screens.AddPost.route) {
                            AddPost(
                                userId = googleAuthUIClient.getSignedInUser()?.userId.orEmpty(),
                                navController = navController
                            )
                        }
                        composable(Screens.AllPostListings.route) {
                            PostListingsScreen(
                                userId = googleAuthUIClient.getSignedInUser()?.userId.orEmpty(),
                                navController = navController
                            )
                        }
                        composable(Screens.SingleMap.route) {
                            SingleMapScreen(
                                userId = googleAuthUIClient.getSignedInUser()?.userId.orEmpty(),
                                navController = navController
                            )
                        }
                    }
                }
            }
        }
    }
}