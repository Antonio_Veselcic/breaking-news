package com.example.breakingnews.di

import com.example.breakingnews.data.repository.PostRepositoryImplementation
import com.example.breakingnews.domain.repository.PostRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindPostRepository(
        postRepository: PostRepositoryImplementation,
    ): PostRepository
}
