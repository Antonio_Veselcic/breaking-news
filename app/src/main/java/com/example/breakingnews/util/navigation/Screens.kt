package com.example.breakingnews.util.navigation

sealed class Screens(val route: String) {
    object AddPost: Screens("add_post")
    object AllPostListings: Screens("all_posts")
    object Profile: Screens("profile")
    object SignIn: Screens("sign_in")
    object SingleMap: Screens("single_map")
}
