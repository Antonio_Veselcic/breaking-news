package com.example.breakingnews.data.data_types

import java.util.Calendar
import java.util.Date

data class PostEntity(
    val userID: String,
    val imageUri: String,
    val title: String,
    val description: String?,
    val latitude: Double,
    val longitude: Double,
    val showLocation: Boolean,
    val date: Date
) {
    constructor() : this("", "", "", "", -1000.0, -1000.0, false, Calendar.getInstance().time)
}
