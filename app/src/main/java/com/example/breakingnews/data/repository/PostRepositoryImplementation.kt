package com.example.breakingnews.data.repository

import android.util.Log
import com.example.breakingnews.data.data_types.PostEntity
import com.example.breakingnews.data.mapper.toPost
import com.example.breakingnews.data.remote.authentication.UserData
import com.example.breakingnews.data.remote.database.FirebaseDatabase
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.data.remote.location.LocationApi
import com.example.breakingnews.domain.model.Post
import com.example.breakingnews.domain.repository.PostRepository
import com.example.breakingnews.util.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostRepositoryImplementation @Inject constructor(
    private val api: LocationApi,
    private val database: FirebaseDatabase,
) : PostRepository {

    override suspend fun getPosts(
        userID: String?,
        location: Location,
    ): Flow<Resource<List<Post>>> {

        var delta = 1000.0
        if (location.longitude == 10.0 || location.latitude == 10.0) {
            delta = 1000.0
        }

        return flow {
            emit(Resource.Loading(true))
            val unfilteredPosts = database.getPosts(userID, location)
            val posts = mutableListOf<PostEntity>()
            delay(1000L)
            if (unfilteredPosts.isNotEmpty()) {

                Log.d("", ""+ unfilteredPosts.size)
                for (postEntity in unfilteredPosts) {
                    posts.add(postEntity)
                }

                emit(
                    Resource.Success(
                        data = posts.map { it.toPost() },
                    ),
                )
            } else {
                emit(Resource.Error(null, "Couldn't get data"))
            }
        }
    }

    override fun uploadPost(postEntity: PostEntity) {
        database.uploadPost(postEntity)
    }

    override fun deletePost(location: Location) {
        database.deletePost(location)
    }

    override fun uploadContacts(userID: String, contacts: String) {
        database.uploadContacts(userID, contacts)
    }

    override fun uploadProfile(userID: String, profile: UserData) {
        database.uploadProfile(userID, profile)
    }

    override fun uploadLocation(userID: String, location: Location) {
        database.uploadLocation(userID, location)
    }
}
