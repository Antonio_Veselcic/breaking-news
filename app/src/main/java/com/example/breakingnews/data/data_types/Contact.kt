package com.example.breakingnews.data.data_types

data class Contact(
    val name: String,
    val phoneNumber: String
)
