package com.example.breakingnews.data.remote.location

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query


// ovo nista ne radi jer sam umjesto ispisa adrese stavio prikaz karte
interface LocationApi {

    @GET("reverse?")
    suspend fun getAddressFromLocation(
        @Query("apiKey") apiKey: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
    ): ResponseBody

    companion object {
        const val API_KEY = "c027fe52b00e42f59ecc4d6b8d030bce"
        const val BASE_URL = "https://api.geoapify.com/v1/geocode/"
    }
}
