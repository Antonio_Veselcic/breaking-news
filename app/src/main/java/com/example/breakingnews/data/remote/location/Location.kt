package com.example.breakingnews.data.remote.location

import com.squareup.moshi.Json

data class Location(
    val latitude: Double,
    val longitude: Double,
)

data class LocationText(
    @field:Json(name = "formatted") val text: String,
)
