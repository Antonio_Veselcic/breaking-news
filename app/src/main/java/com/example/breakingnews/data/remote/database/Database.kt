package com.example.breakingnews.data.remote.database

import com.example.breakingnews.data.data_types.PostEntity
import com.example.breakingnews.data.remote.authentication.UserData
import com.example.breakingnews.data.remote.location.Location

interface Database {

    fun uploadPost(postEntity: PostEntity)

    suspend fun getPosts(userID: String?, location: Location): List<PostEntity>

    fun deletePost(location: Location)

    fun uploadContacts(userID: String, contacts: String)

    fun uploadProfile(userID: String, profile: UserData)

    fun uploadLocation(userID: String, location: Location)
}
