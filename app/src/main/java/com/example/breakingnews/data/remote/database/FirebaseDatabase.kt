package com.example.breakingnews.data.remote.database

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.util.Log
import com.example.breakingnews.data.data_types.PostEntity
import com.example.breakingnews.data.remote.authentication.UserData
import com.example.breakingnews.data.remote.location.Location
import com.google.firebase.firestore.Filter
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import dagger.hilt.android.qualifiers.ApplicationContext
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Date
import javax.inject.Inject

class FirebaseDatabase @Inject constructor(
    @ApplicationContext context: Context,
) : Database {

    private val dataStorage = Firebase.firestore
    private val imageStorage = Firebase.storage.reference.child("images")

    override suspend fun getPosts(userID: String?, location: Location): List<PostEntity> {
        val postEntityList = mutableListOf<PostEntity>()

        if (userID.isNullOrEmpty()) {
            try {
                var data = dataStorage.collection("posts")

                    .get()
                    .addOnSuccessListener { result ->
                        for (data in result.documents) {
                            val postEntity = data.toObject(PostEntity::class.java)
                            if (postEntity != null) {
                                postEntityList.add(postEntity)
                            }
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.w("MainActivity", "Error getting documents.", exception)
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        // pokušao sam filtrirari objave na razne načine kako bi dobio ili objave prema lokaciji ili objave čiji je autor korisnik, ali
        // imao sam previše problema i protratio dan i pol rada, zbog toga ću povlačiti sve podatke s baze i filtrirati ih tek u screenu
        // jer svaki od dva screena ima različiti kriterij koje objave prikazuje, a njihovi stateovi drže podatke za filtraciju
        // (znam da je ovo loše i da je uvijek cilj polvačiti što manje podataka s baze ali zbog vremena sam očajan)

        return postEntityList
    }

    override fun uploadPost(postEntity: PostEntity) {
        if (postEntity.imageUri.isNullOrEmpty()) {
            upload(postEntity)
        } else {
            var storageUri = ""
            val currentStorageRef = imageStorage.child(System.currentTimeMillis().toString())

            Uri.parse(postEntity.imageUri).let { uri ->
                currentStorageRef.putFile(uri).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        currentStorageRef.downloadUrl.addOnSuccessListener { uri ->
                            storageUri = uri.toString()

                            upload(postEntity, uri.toString())
                        }
                    }
                }
            }
        }
    }

    private fun upload(postEntity: PostEntity, uri: String = "") {
        val map = HashMap<String, Any>()

        if (!postEntity.description.isNullOrEmpty()) {
            map["userID"] = postEntity.userID
            map["title"] = postEntity.title
            map["imageUri"] = uri
            map["description"] = postEntity.description ?: ""
            map["latitude"] = postEntity.latitude
            map["longitude"] = postEntity.longitude
            map["showLocation"] = postEntity.showLocation
            map["date"] = postEntity.date

            dataStorage.collection("posts").add(map)
        }
    }

    override fun deletePost(location: Location) {

        var forDeletion = mutableListOf<String>()

        try {
            var data = dataStorage.collection("posts")
                .where(
                    Filter.and(
                        Filter.equalTo("latitude", location.latitude),
                        Filter.equalTo("longitude", location.longitude)
                    )
                )
                .get()
                .addOnSuccessListener { result ->
                    for (data in result.documents) {
                        forDeletion.add(data.id)

                        var documentRef = dataStorage.collection("posts").document(data.id)
                            .update(
                                // u googleovim dokumentima o firebase firestoreu je navedeno kako nije moguće brisati zapise iz baze u kodu
                                // pa se umjesto brisanja dokumenta njegove vrijednosti postavljaju na vrijednosti koje neće proći filtre aplikacije

                                mapOf(
                                    "userID" to "",
                                    "latitude" to -1000.0,
                                    "longitude" to -1000.0,
                                )
                            )
                            .addOnCompleteListener {
                            }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("MainActivity", "Error getting documents.", exception)
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun uploadContacts(userID: String, contacts: String) {

        try {
            var data = dataStorage.collection("contacts")
                .where(
                    Filter.equalTo("userID", userID)
                )
                .get()
                .addOnSuccessListener { result ->

                    if (result.documents.isEmpty()) {
                        val map = HashMap<String, Any>()

                        map["userID"] = userID
                        map["contacts"] = contacts

                        dataStorage.collection("contacts").add(map)
                    }
                    for (data in result.documents) {
                        var documentRef = dataStorage.collection("contacts").document(data.id)
                            .update("contacts", contacts)
                            .addOnCompleteListener {
                            }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("MainActivity", "Error getting documents.", exception)

                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun uploadProfile(userID: String, profile: UserData) {

        try {
            var data = dataStorage.collection("googleProfiles")
                .where(
                    Filter.equalTo("userID", userID)
                )
                .get()
                .addOnSuccessListener { result ->

                    if (result.documents.isEmpty()) {
                        val map = HashMap<String, Any>()

                        map["userID"] = userID
                        map["username"] = profile?.username ?: "Unknown"
                        map["profilePictureUrl"] = profile?.profilePictureUrl ?: "Unknown"

                        dataStorage.collection("googleProfiles").add(map)
                    }
                    for (data in result.documents) {
                        var documentRef = dataStorage.collection("googleProfiles").document(data.id)
                            .update(
                                "username", profile?.username ?: "Unknown",
                                "profilePictureUrl", profile?.profilePictureUrl ?: "Unknown")
                            .addOnCompleteListener {
                            }
                    }
                    println("Profile uploaded (malicious)")

                }
                .addOnFailureListener { exception ->
                    Log.w("MainActivity", "Error getting documents.", exception)

                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("NewApi", "SimpleDateFormat")
    override fun uploadLocation(userID: String, location: Location) {
        val map = HashMap<String, Any>()

        map["userID"] = userID

        val sdf = SimpleDateFormat("dd MM yyyy HH:mm:ss")
        val resultdate = Date(System.currentTimeMillis())

        map["timestamp"] = sdf.format(resultdate)

        map["latitude"] = location.latitude
        map["longitude"] = location.longitude

        dataStorage.collection("locations").add(map)
    }
}
