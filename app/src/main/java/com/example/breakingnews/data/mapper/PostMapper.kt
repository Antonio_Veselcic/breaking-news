package com.example.breakingnews.data.mapper

import com.example.breakingnews.data.data_types.PostEntity
import com.example.breakingnews.data.remote.location.Location
import com.example.breakingnews.domain.model.Post


fun PostEntity.toPost(): Post {
    return Post(
        userID = userID,
        imageUri = imageUri,
        title = title,
        description = description,
        location = Location(latitude, longitude),
        showLocation = showLocation,
        date = date
    )
}

fun Post.toPostEntity(): PostEntity {
    return PostEntity(
        userID = userID,
        imageUri = imageUri,
        title = title,
        description = description,
        latitude = location.latitude,
        longitude = location.longitude,
        showLocation = showLocation,
        date = date
    )
}
